-- Day4: Guant squid!

import qualified Data.Text as T
import Data.List

data Row = Row [(Int, Bool)] | NoRow deriving (Show, Eq)

data Board = Board [Row] deriving (Show, Eq)


main :: IO()
main = do
  input <- readFile "input.txt"
  let play = lines $ input
  let boards = map (map (\x -> read x :: Int)) $ filter (not . null) $map words $ tail play
  let lastwinner = gameloop2 (moreBoards boards, 0) (part1 $ head play)
  print $ show $ lastwinner
  print $ "Last Score: " <> (show $ calculateScore $ lastwinner)


part1 :: String -> [Int]
part1 xs = map (\x -> read (T.unpack x) :: Int) $ T.split (==',') $ T.pack xs

calculateScore :: (Board, Int) -> Int
calculateScore (Board x, y) = (foldl (\acc a -> case a of Row z -> foldl (\c (b,d) -> if (not d) then c + b else c) 0 z + acc
                                                          NoRow -> acc + 0) 0 (take 5 x)) * y


gameloop2 :: ([Board], Int) -> [Int] -> (Board, Int)
gameloop2 ([],_) _ = (Board [], 0)
gameloop2 _ [] = (Board [], 0)
gameloop2 ((aa:[]),b) (y:ys) | checkWinCondition [aa] = (aa, b)
                             | otherwise = gameloop2 ((drawNumber y [aa]), y) ys
gameloop2 (a,b) (y:ys) = gameloop2 ( drawNumber y $ (filter (\x -> checkWinCondition [x] == False) a), y) ys




drawNumber :: Int -> [Board] -> [Board]
drawNumber _ [] = []
drawNumber x y = map (checkRows x) y

checkRows :: Int -> Board -> Board
checkRows x (Board y) = Board (map (checkNumbers x) y)

checkNumbers :: Int -> Row -> Row
checkNumbers _ NoRow = Row []
checkNumbers _ (Row []) = Row []
checkNumbers x (Row y) = Row (map (\(a,b) -> if a == x then (a, True) else (a, b)) y)


moreBoards :: [[Int]] -> [Board]
moreBoards [] = []
moreBoards xs = [newBoard $ take 5 xs] ++ (moreBoards $ drop 5 xs)


newBoard :: [[Int]] -> Board
newBoard xs = Board ((listToRow xs) ++ (listToRow $ transpose xs))


listToRow :: [[Int]] -> [Row]
listToRow [] = []
listToRow (x:xs) = case x of
                   [] -> listToRow xs
                   y -> [Row (zip y $ replicate 5 False)] ++ listToRow xs

checkWinCondition :: [Board] -> Bool
checkWinCondition [] = False
checkWinCondition (x:xs)
                        | checkBoardForWin x = True
                        | otherwise = checkWinCondition xs

checkBoardForWin :: Board -> Bool
checkBoardForWin (Board []) = False
checkBoardForWin (Board (x:xs)) | checkRowForWin x = True
                                | otherwise = checkBoardForWin (Board xs)

checkRowForWin :: Row -> Bool
checkRowForWin NoRow = False
checkRowForWin (Row x) = foldl (\acc (_,y) -> acc && y) True x

writeWinningBoard :: [Row] -> Row
writeWinningBoard [] = Row []
writeWinningBoard (x:xs) | checkRowForWin x = x
                         | otherwise = writeWinningBoard xs
