-- Day 7 of advent of code 2021

findMinimumCost :: [Int] -> Int
findMinimumCost x = go (defineAverage x) x
                  where
                    go i y | z0 < z1 && z0 < z2 = z0
                           | defineFuelCost y (i) > defineFuelCost (y) (i-1) = go (i-1) y
                           | defineFuelCost y (i) < defineFuelCost y (i-1) = go (i+1) y
                           where
                             z0 = defineFuelCost y (i)
                             z1 = defineFuelCost y (i+1)
                             z2 = defineFuelCost y (i-1)



defineFuelCost :: [Int] -> Int -> Int
defineFuelCost x y = sum $ [gaußianSum (abs (i-y)) | i <- x]
                where gaußianSum z = div (z^2 + z) 2

defineAverage :: [Int] -> Int
defineAverage x = div (sum x) (length x)

parseInput :: String -> [Int]
parseInput x = read $ "[" ++ x ++ "]"

main :: IO()
main = do
  readOut <- readFile "input.txt"
  print $ show $ findMinimumCost $ parseInput readOut
