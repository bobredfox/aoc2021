-- Solution for Day 6 of Advent of Code

main :: IO()
main = do
  inputs <- readFile "input.txt"
  print $ sum $ flip toDayX 256 $ newDay $ newColony $ parseInput inputs

-- Parses the Inputstring in Format: Int,Int,Int...,Int to [Int]
parseInput :: String -> [Int]
parseInput x = read $ "[" ++ x ++ "]"

-- Groups the different "laternfish" by Age
newColony :: [Int] -> [Int]
newColony x = [ length $ filter (==j) x | j <- [0..8]]

-- Iterates a new day
newDay :: [Int] -> [Int]
newDay [a0,a1,a2,a3,a4,a5,a6,a7,a8] = [a1,a2,a3,a4,a5,a6,a7 + a0, a8, a0]
newDay _ = []

-- Iterates lazy to day y and takes the last state
toDayX :: [Int] -> Int -> [Int]
toDayX x y = last $ take y $ iterate newDay x
