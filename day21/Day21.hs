-- Advent of Code Day 21 dice!

module Day21 where

import Control.Monad.State
import Debug.Trace

main :: IO()
main = do
  let start1 = 1
  let start2 = 5

data Game = Game {
  posPlayer1 :: Int ,
  posPlayer2 :: Int ,
  scorePlayer1 :: Int ,
  scorePlayer2 :: Int ,
  dice :: Int ,
  diceCount :: Int ,
  active :: Player} deriving Show

data Player = Player1 | Player2 deriving Show

initGame :: Int -> Int -> Game
initGame p1 p2 = Game p1 p2 0 0 0 0 Player1

rollDice :: State Game ()
rollDice = do
  game <- get
  let newDice = if dice game == 100 then 1 else dice game + 1
  put $ game { dice = newDice, diceCount = (diceCount game + 1) }

playerTurn :: State Game ()
playerTurn = do
  list <- replicateM 3 (do
                           rollDice
                           updatePos)
  updateScore (last list)

updatePos :: State Game Int
updatePos = do
  game <- get
  case (active game) of Player1 -> do
                          let newPos = findNewPos (posPlayer1 game) (dice game)
                          put $ game {posPlayer1 = newPos}
                          return newPos
                        Player2 -> do
                          let newPos = findNewPos (posPlayer2 game) (dice game)
                          put $ game {posPlayer2 = newPos}
                          return newPos

updateScore :: Int -> State Game ()
updateScore x = do
  game <- get
  case active game of Player1 -> put $ game {scorePlayer1 = scorePlayer1 game + x}
                      Player2 -> put $ game {scorePlayer2 = scorePlayer2 game + x}

findNewPos :: Int -> Int -> Int
findNewPos x y = if newValue > 10 then newPos
                                 else newValue
                where newValue = x + y
                      newPos | mod newValue 10 == 0 = 10
                             | otherwise = mod newValue 10

switchPlayer :: Player -> Player
switchPlayer Player1 = Player2
switchPlayer Player2 = Player1

newPlayer :: State Game ()
newPlayer = do
  game <- get
  let nextPlayer = switchPlayer (active game)
  put $ game {active = nextPlayer}

reachedEnd :: State Game Bool
reachedEnd = do
  game <- get
  if ((scorePlayer1 game) >= 1000) || ((scorePlayer2 game) >= 1000) then return True else return False


fullTurn :: State Game Int
fullTurn = do
  playerTurn
  newPlayer
  status <- reachedEnd
  game <- get
  trace ("Checking status " ++ show game) (if status then return status else fullTurn)
  return status
