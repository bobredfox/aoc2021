
import Numeric
import Data.Char (digitToInt)
import Data.List (transpose)


main :: IO()
main = do
  inputs <- readFile "input.txt"
  let inputData = lines inputs
  let transposed = transpose $ lines inputs
  print $ most inputData
  print $ part2 inputData

part1 :: [[Char]] -> Int
part1 xs = (binaryToInt $ map (head) lists) * (binaryToInt $ map (last) lists)
      where lists = map (\x -> if x > 500 then [1,0] else [0,1]) $ map (length . filter (=='1')) xs
-- multiplying oxygen generator rating with co2 scrubber rating

part2 :: [String] -> Int
part2 input = (binToInt (oxy input)) * (binToInt (co2 input))

oxy :: [String] -> String
oxy = go 0
      where
        go _ [x] = x
        go n xs = go (n+1) (filter (\x -> x!!n == ((most xs)!!n)) xs)

co2 :: [String] -> String
co2 = go 0
      where
        go _ [x] = x
        go n xs = go (n+1) (filter (\x -> x!!n == ((least xs)!!n)) xs)

most :: [String] -> String
most xs = foldl (\acc x -> acc ++ mostObject x) [] $ transpose xs

least :: [String] -> String
least xs = foldl (\acc x -> acc ++ leastObject x) [] $ transpose xs

mostObject :: String -> String
mostObject a
            | length0 > length1 = "0"
            | otherwise = "1"
  where length0 = length $ filter (=='0') a
        length1 = length $ filter (=='1') a

leastObject :: String -> String
leastObject a
            | mostObject a == "1" = "0"
            | mostObject a == "0" = "1"

binToInt :: String -> Int
binToInt [] = 0
binToInt (x:xs) = digitToInt x * (2 ^ length xs) + binToInt xs


binaryToInt :: [Int] -> Int
binaryToInt [] = 0
binaryToInt (x:xs) = x * (2 ^ length xs) + binaryToInt xs
