

main :: IO()
main = do
  reading <- readFile "input.txt"
  let instructions = map words $ lines reading
  let tuple = map (\x -> (head x, read (last x) :: Int)) instructions
  print $ "Part 1: " <> show (uncurry (*) $ part1 tuple)
  print $ "Part 2: " <> show ((\(x,y,_) -> x * y) $ part2 tuple)

part1 :: [(String,Int)] -> (Int,Int)
part1 xs = foldl acc (0,0) xs
          where acc (x,y) (x',y') = case x' of "forward" -> (x + y', y)
                                               "down" -> (x, y + y')
                                               "up" -> (x, y - y')
                                               otherwise -> (x,y)

part2 :: [(String,Int)] -> (Int, Int, Int)
part2 xs = foldl acc (0, 0, 0) xs
           where acc (x, y, z) (ins, val) = case ins of "forward" -> (x + val, y + (val * z), z)
                                                        "down" -> (x, y, z + val)
                                                        "up" -> (x, y, z - val)
                                                        otherwise -> (x,y,z)
