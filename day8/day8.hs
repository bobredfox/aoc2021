-- Advent of code day 8

import qualified Data.Text as T
import Data.List


main :: IO()
main = do
  digits <- readFile "input.txt"
  let parsedDigits = map (parseDigits) $ lines digits
  print $ "Part 1: " <> (show $ sum $ map uniqueDigits $ map (\(_,y) -> y) parsedDigits)
  print $ "Part 2: " <> (show $ sum $ map (\x -> (puzzleTogether $ map (snd) x)) $ map (drop 10) $ map (partf) $ map (uniqueDigitsChars) $ map (\(x,y) -> x ++ y) parsedDigits)

parseDigits :: String -> ([String], [String])
parseDigits x = (words $ head $ splitted x, words $ last $ splitted x)
            where splitted y = map (\a -> T.unpack a) $ T.splitOn (T.pack "|") (T.pack y)

uniqueDigits :: [String] -> Int
uniqueDigits x = sum [ isUnique y | y <- x]
             where isUnique a | length a >= 2 && length a <= 4 = 1
                              | length a == 7 = 1
                              | otherwise = 0

uniqueDigitsChars :: [String] -> [(String, Int)]
uniqueDigitsChars x = map (\y -> (y, isUnique y)) x
                      where isUnique a | alength == 2 = 1
                                       | alength == 3 = 7
                                       | alength == 4 = 4
                                       | alength == 7 = 8
                                       | otherwise = -1
                                       where alength = length a

partf :: [(String, Int)] -> [(String, Int)]
partf x = map (theNumbers) x
  where thediff p q = fst $ head (filter (\(a,b) -> b == p) q)
        theL = (thediff 4 x) \\ (thediff 1 x)
        the4 = thediff 4 x
        the7 = thediff 7 x
        theNumbers (a,b) | length a == 6 = (a, sixChars a theL the4)
                         | length a == 5 = (a, fiveChars a theL the7)
                         | otherwise = (a,b)

-- Determines the number from 5 Char numbers (2,3,5)
fiveChars :: String -> String -> String -> Int
fiveChars x l s | theLinside = 5
                | the7inside = 3
                | otherwise = 2
               where theLinside = foldl (\acc z -> (elem z x) && acc) True l
                     the7inside = foldl (\acc z -> (elem z x) && acc) True s

-- Determines the number from 6 Char numbers (0,6,9)
sixChars :: String -> String -> String -> Int
sixChars x l v | not theLinside = 0
               | not the4inside = 6
               | otherwise = 9
        where theLinside = foldl (\acc z -> (elem z x) && acc) True l
              the4inside = foldl (\acc z -> (elem z x) && acc) True v


puzzleTogether :: [Int] -> Int
puzzleTogether [a,b,c,d] = read ( show a ++ show b ++ show c ++ show d )
puzzleTogether _ = 0
