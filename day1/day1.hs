
main :: IO()
main = do
  readOuts <- readFile "input.txt"
  let readInput = map (read) (lines $ readOuts) :: [Integer]
  let ente = foldl (\acc p -> case p of {True -> acc + 1; False -> acc}) 0 $ map (\(x, y) -> y > x) $ zip readInput $ tail readInput
  let ente2 = map (\(x, y, z) -> x + y + z) $ zip3 readInput (tail readInput) (tail $ tail readInput)
  let ente3 = foldl (\acc p -> case p of {True -> acc + 1; False -> acc}) 0 $ map (\(x, y) -> y > x) $ zip ente2 $ tail ente2
  putStrLn $ show ente3
  putStrLn $ show $ countDepth $ triplePair readInput

countDepth :: [Integer] -> Int
countDepth [] = 0
countDepth [_] = 0
countDepth (x:y:xs)
                   | y > x = 1 + countDepth (y:xs)
                   | otherwise = countDepth (y:xs)


triplePair :: [Integer] -> [Integer]
triplePair [] = []
triplePair (_:[]) = []
triplePair (_:_:[]) = []
triplePair (x:y:z:xs) = (x+y+z) : triplePair (y:z:xs)
