{-# LANGUAGE NamedFieldPuns #-}

import Data.Array

data Vent = Vent {x1 :: Int, y1::Int, x2::Int, y2::Int} deriving (Show)

main :: IO()
main = do
  readings <- readFile "input.txt"
  let field = array ((0,0), (999,999)) [((i,j), 0) | i <- [0..999], j<-[0..999]]
  print $ map parseVent $ lines readings
  print $ detCrossings $ updateMap field $ map parseVent $ lines readings

parseVent :: String -> Vent
parseVent s = Vent {x1, y1, x2, y2} where
  [p1,_,p2] = words s
  (x1,y1) = read $ "(" ++ p1 ++ ")"
  (x2,y2) = read $ "(" ++ p2 ++ ")"

updateMap :: Array (Int, Int) Int -> [Vent] -> Array (Int, Int) Int
updateMap x [] = x
updateMap x (v:vs) = updateMap (x // inputlist v) vs
                where
                  inputlist y | x1 y == x2 y = [((x1 y, i), x!(x1 y, i)+1)| i <- [(min b1 b2)..(max b1 b2)]]
                              | y1 y == y2 y = [((i, y1 y), x!(i, y1 y)+1)| i <- [(min a1 a2)..(max a1 a2)]]
                              | otherwise = [(i, x!i + 1) | i <- buildDiag (a1, b1) (a2, b2)]
                              where a1 = x1 y
                                    a2 = x2 y
                                    b1 = y1 y
                                    b2 = y2 y

detCrossings :: Array (Int, Int) Int -> Int
detCrossings = foldl (\acc x -> if x > 1 then acc + 1 else acc) 0

buildDiag :: (Int, Int) -> (Int, Int) -> [(Int, Int)]
buildDiag (a,b) (c,d) | a < c = if b < d then [(a+x,b+x)| x <- [0..(c-a)]] else [(a+x, b-x)| x <-[0..(c-a)]]
                      | a > c = if b < d then [(a-x,b+x)| x <- [0..(a-c)]] else [(a-x, b-x)| x <-[0..(a-c)]]
                      | otherwise = []
